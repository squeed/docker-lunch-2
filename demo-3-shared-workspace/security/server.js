var express = require('express');
var app = express();
var fs = require('fs');

app.get('/', function (req, res) {
  console.log("Security got a request!")
  var data = '';
  var readStream = fs.createReadStream('workspace/data.file', 'utf8');

  readStream.on('data', function(chunk) {
      data += chunk;
  }).on('end', function() {
      res.send(data)
  });
});

app.listen(3000, function () {
  console.log('Security app listening on port 3000!');
});
