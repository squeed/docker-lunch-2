var express = require('express');
var app = express();
var axios = require('axios');

var url = process.env.EXT_URL;

app.get('/', function (req, res) {
  res.send('Hello from the webapp!');
});

app.get('/security', function (req, res) {
  console.log("Target url: " + url);
  axios.get(url)
  .then(function(response){
      res.send(response.data);
  }).catch(function (error) {
      console.log(error);
  });
});

app.listen(3000, function () {
  console.log('Webapp listening on port 3000!');
});
