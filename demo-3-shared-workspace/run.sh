#Cleanup from previous runs
docker kill webapp security; docker rm webapp security;  docker network rm demo2_network

#Create a network
docker network create demo2_network &&

#Build images
docker build -t demo2-webapp webapp &&
docker build -t demo2-security security &&

#Start the containers and create them to the network
docker run -d --net demo2_network -p 3000:3000 --name webapp demo2-webapp &&

# Also mount the workspace-folder to the container
docker run -d --net demo2_network -v /Users/ola/repos/docker-demo/demo-3-shared-workspace/security/workspace:/usr/home/docker/workspace --name security demo2-security
