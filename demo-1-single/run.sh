#Cleanup from previous runs
docker kill demo1-webapp;
docker rm demo1-webapp;

#Build image with tag demo1-webapp
docker build -t demo1-webapp . &&

#Run container, port forward 3000 -> 3000 to the host, name the container demo1-webapp
docker run -d -p 3000:3000 --name demo1-webapp demo1-webapp
