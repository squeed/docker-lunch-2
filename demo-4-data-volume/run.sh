#Cleanup
docker kill persistdata ; docker rm persistdata; docker volume rm datavolume;

#Create a docker volume
docker volume create --name datavolume && 

#Map volume to your container, when executing this you will land in shell (/)
# do "date >> /tmp/persist" and then kill the container. Restart an instance
# and do the same. You should now see two entries in the persist-file
docker run -ti -v datavolume:/tmp --name persistdata busybox
