var express = require('express');
var app = express();

app.get('/', function (req, res) {
  console.log("Security got a request!")
  res.send('This was sent from the security container');
});

app.listen(3000, function () {
  console.log('Security app listening on port 3000!');
});
