#Cleanup from previous runs
docker kill webapp security ; docker rm webapp security ; docker network rm demo2_network

#Create a network
docker network create demo2_network &&

#Build images
docker build -t demo2-webapp webapp/ &&
docker build -t demo2-security security/ &&

#Start the containers and connect them to the network
docker run -d --net demo2_network -p 3000:3000 --name webapp demo2-webapp &&
docker run -d --net demo2_network --name security demo2-security
